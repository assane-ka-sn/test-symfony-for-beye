<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Brand;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * Brand controller.
 *
 * @Route("brands")
 */
class BrandController extends FOSRestController
{
    /**
     * BrandController constructor.
     */

    /**
     * Lists all brands entities.
     *
     * @View()
     * @Get("/")
     */
    public function indexAction()
    {
        $brands = $this->getDoctrine()->getRepository('AppBundle:Brand')->findAll();

        return $brands;
    }

    /**
     * Finds and displays a brand entity.
     *
     * @View()
     * @Get(path = "/{brandId}", name = "brand_show", requirements = {"brandId"="\d+"})
     */
    public function showAction(Request $request)
    {
        $brand = $this->getDoctrine()->getRepository('AppBundle:Brand')->find($request->get('brandId'));
        if (empty($brand)) {
            return new JsonResponse(['message' => 'Brand not found'], Response::HTTP_NOT_FOUND);
        }
        return $brand;
    }

    /**
     * Creates a new brand entity.
     *
     * @View()
     * @Post(path = "", name = "brand_new")
     * @ParamConverter("brand", converter="fos_rest.request_body")
     */
    public function newAction(Brand $brand)
    {
        $brandNew = $this->getDoctrine()->getRepository('AppBundle:Brand')->findOneByName($brand->getName());

        if (empty($brandNew)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($brand);
            $em->flush();

            $brandNew = $brand;
        }

        return $this->redirectToRoute('brand_show', array('brandId' => $brandNew->getId()));
    }

    /**
     * Edit an existing brand entity
     *
     * @View()
     * @PUT(path = "/{brandId}", name = "brand_edit", requirements = {"brandId"="\d+"})
     */
    public function editAction(Request $request)
    {
        $brandEdit = $this->getDoctrine()->getRepository('AppBundle:Brand')->find($request->get('brandId'));

        if (empty($brandEdit)) {
            return new JsonResponse(['message' => 'Brand not found for edit'], Response::HTTP_NOT_FOUND);
        }
        $brandNew = $this->getDoctrine()->getRepository('AppBundle:Brand')->findOneByName($request->get('name'));

        if (empty($brandNew)) {
            $brandEdit->setName($request->get('name'));
            $em = $this->getDoctrine()->getManager();
            $em->merge($brandEdit);
            $em->flush();

            //return $this->redirectToRoute('brand_show', array('brandId' => $brandEdit->getId()))
            return new JsonResponse(['message' => 'Brand edited successfully'], Response::HTTP_OK);

        }

        return new JsonResponse(['message' => 'Brand already exist'], Response::HTTP_NOT_FOUND);
    }


    /**
     * Deletes a brand entity.
     *
     * @View()
     * @Delete(path = "/{brandId}", name = "brand_delete", requirements = {"brandId"="\d+"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $brand = $em->getRepository('AppBundle:Brand')->find($request->get('brandId'));

        if (empty($brand)) {


            return new JsonResponse(['message' => 'Brand not found for delete'],Response::HTTP_NOT_FOUND);
        }

        $products = $em->getRepository('AppBundle:Product')->findByBrand($brand);
        foreach ($products as $product){
            $em->remove($product);
            $em->flush();
        }

        $em->remove($brand);
        $em->flush();
        return new JsonResponse(['message' => 'Brand deleted successfully'], Response::HTTP_OK);
    }


}
