<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Model\RequestNewProduct;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Product controller.
 *
 * @Route("products")
 */
class ProductController extends FOSRestController
{

    /**
     * Lists all product entities.
     *
     * @View()
     * @Get("/")
     */
    public function indexAction()
    {
        $products = $this->getDoctrine()->getRepository('AppBundle:Product')->findAll();

        if (empty($products)) {
            return new JsonResponse(['message' => 'Products not found'], Response::HTTP_NOT_FOUND);
        }

        $formatted = [];
        foreach ($products as $product) {
            $formatted[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'brand' => $product->getBrand(),
                'categories' => $product->getCategories(),
                'url' => $product->getUrl(),
                'md5' => md5($product->getId())
            ];
        }

        return $formatted;
    }

    /**
     * Finds and displays a product entity.
     *
     * @Get(path = "/{productId}", name = "product_show", requirements = {"productId"="\d+"})
     * @View()
     */
    public function showAction(Request $request)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($request->get('productId'));
        if (empty($product)) {
            return new JsonResponse(['message' => 'Product not found'], Response::HTTP_NOT_FOUND);
        }
        $formatted = [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'description' => $product->getDescription(),
            'brand' => $product->getBrand(),
            'categories' => $product->getCategories(),
            'url' => $product->getUrl(),
            'md5' => md5($product->getId())
        ];

        return $formatted;
    }

    /**
     * Creates a new product entity.
     *
     * @View()
     * @Post(path = "", name = "product_new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $brand = $em->getRepository('AppBundle:Brand')->find($request->get('brandId'));

        $product = new Product();
        $product->setName($request->get('name'));
        $product->setDescription($request->get('description'));
        $product->setActive(false);
        $product->setUrl("");
        $product->setBrand($brand);


        $em->persist($product);
        $em->flush();

        $product->setUrl($this->generateUrl('product_show', array('productId' => $product->getId()), UrlGeneratorInterface::ABSOLUTE_URL));
        $em->merge($product);
        $em->flush();

        return $this->redirectToRoute('product_show', array('productId' => $product->getId()));
    }

    /**
     * Edit an existing product entity
     *
     * @View()
     * @Put(path = "/{productId}", name = "product_edit", requirements = {"productId"="\d+"})
     */
    public function editAction(Request $request)
    {

        $productEdit = $this->getDoctrine()->getRepository('AppBundle:Product')->find($request->get('productId'));

        if (empty($productEdit)) {
            return new JsonResponse(['message' => 'Product not found for edit'], Response::HTTP_NOT_FOUND);
        }
        $em = $this->getDoctrine()->getManager();
        $brand = $em->getRepository('AppBundle:Brand')->find($request->get('brandId'));

        $productEdit->setName($request->get('name'));
        $productEdit->setDescription($request->get('description'));
        $productEdit->setBrand($brand);

        $em->merge($productEdit);
        $em->flush();

        //return $this->redirectToRoute('product_show', array('productId' => $productEdit->getId()));
        return new JsonResponse(['message' => 'Product edited successfully'], Response::HTTP_OK);
    }

    /**
     * Edit an existing product entity for add category
     *
     * @View()
     * @Put(path = "/{productId}/category", name = "product_edit_add_category", requirements = {"productId"="\d+"})
     */
    public function editForAddCategoryAction(Request $request)
    {

        $productEdit = $this->getDoctrine()->getRepository('AppBundle:Product')->find($request->get('productId'));

        if (empty($productEdit)) {
            return new JsonResponse(['message' => 'Product not found for edit'], Response::HTTP_NOT_FOUND);
        }
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($request->get('categoryId'));

        $productEdit->addCategory($category);

        $em->merge($productEdit);
        $em->flush();

        //return $this->redirectToRoute('product_show', array('productId' => $productEdit->getId()));
        return new JsonResponse(['message' => 'Product edited successfully'], Response::HTTP_OK);
    }

    /**
     * Deletes a product entity.
     *
     * @View()
     * @Delete(path = "/{productId}", name = "product_delete", requirements = {"productId"="\d+"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('AppBundle:Product')->find($request->get('productId'));

        if (empty($product)) {
            return new JsonResponse(['message' => 'Product not found for delete'],Response::HTTP_NOT_FOUND);
        }

        $em->remove($product);
        $em->flush();

        return new JsonResponse(['message' => 'Product deleted successfully'], Response::HTTP_OK);
    }

}
